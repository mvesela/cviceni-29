/* eslint-disable brace-style */
import hotkeys from 'hotkeys-js';
import {
  __hasClass, __addClass, __removeClass, __dispatchEvent,
} from '../lib/utils';
import Cviceni from './cviceni';

// Description:
// ======================================================
// Navigation
// consists from:
// - prev/next button
// - history
// affects:
// - slides
// - pagination (generated in build process)
// - prev/next button (moved inside slides)
// - history
// ——————————————————————————————————————————————————————
// .slide CSS classes
// - .active-slide
// - .previous-slide
// - .next-slide
// - .visited-slide
// - .half-slide-active (.slide is on the right when is .active-slide)
// - .half-slide-previous (.slide is on the left when .previous-slide)
// - .previous-to-active-slide (mark the most recently viewed .slide)
// ======================================================

export default class Navigation extends Cviceni {
  constructor() {
    super();
    // bind the context of an event listener
    this._clickPrev = this._clickPrev.bind(this);
    this._clickNext = this._clickNext.bind(this);

    // fix tabbing
    document.querySelectorAll('div').forEach(($div) => {
      $div.setAttribute('tabindex', -1);
      $div.style.outline = '0';
    });

    // History (back & forward buttons)
    // A popstate event is dispatched to the window each time the active history entry changes between two history entries for the same document. If the activated history entry was created by a call to history.pushState(), or was affected by a call to history.replaceState(), the popstate event's state property contains a copy of the history entry's state object.
    // https://developer.mozilla.org/en-US/docs/Web/API/WindowEventHandlers/onpopstate
    window.onpopstate = (event) => {
      this.analytics = HISTORYLAB.analytics.data;
      const $activeSlide = this.$cviceni.querySelector('.active-slide');
      const activeSlideIndex = $activeSlide.getAttribute('data-slide-index');
      const $newSlide = this.$cviceni.querySelector(window.location.hash);
      const newSlideIndex = $newSlide.getAttribute('data-slide-index');

      // check if the slide in url was already visited to prevent user to see slides which wasnt visited yet
      // AND
      // check if the new slide isnt tha same one
      if (($newSlide && __hasClass($newSlide, 'visited-slide')) && ($newSlide.id !== $activeSlide.id)) {
        __removeClass($activeSlide, 'active-slide');

        Navigation.enableTabControlOnActiveSlide($newSlide, $activeSlide);

        // zaznam do analytiky:
        this.analytics.activity.length += 1;
        this.analytics.activity.steps[this.analytics.activity.length] = {
          slide: parseInt(newSlideIndex, 10),
          time: Math.round(performance.now()) - this.analytics.time.start.time,
          date: Date.now(),
          interaction: {
            type: event.type,
          },
        };

        // going back
        if (activeSlideIndex > newSlideIndex) {
          this.analytics.activity.steps[this.analytics.activity.length].type = 'P';
          this.analytics.activity.steps[this.analytics.activity.length].interaction.name = 'back';

          // slide
          __addClass($activeSlide, 'next-slide');
          __removeClass($newSlide, 'previous-slide');

          if (__hasClass($newSlide, 'previous-to-active-slide')) {
            __removeClass($newSlide, 'previous-to-active-slide');
          }
          if (__hasClass($newSlide.previousElementSibling, 'half-slide-previous')) {
            __addClass($newSlide.previousElementSibling, 'previous-to-active-slide');
          }
        }
        // going forward
        else {
          this.analytics.activity.steps[this.analytics.activity.length].type = 'D';
          this.analytics.activity.steps[this.analytics.activity.length].interaction.name = 'forward';

          // slides
          __addClass($activeSlide, 'previous-slide');
          __removeClass($newSlide, 'next-slide');

          if (__hasClass($activeSlide, 'half-slide-previous')) {
            __addClass($activeSlide, 'previous-to-active-slide');
          }
          if (__hasClass($activeSlide.previousElementSibling, 'previous-to-active-slide')) {
            __removeClass($activeSlide.previousElementSibling, 'previous-to-active-slide');
          }
        }

        __addClass($newSlide, 'active-slide');

        // dispatch event that slide has changed
        __dispatchEvent(this.$cviceni, 'slide.change', {}, { slide: { active: this.getActiveSlide($newSlide) } });

      } else if (!__hasClass($newSlide, 'visited-slide')) {
        const activeSlideHash = `#${$activeSlide.id}`;
        if (window.history.replaceState) {
          window.history.replaceState(null, null, activeSlideHash);
        } else {
          window.location.hash = activeSlideHash;
        }
      }

      HISTORYLAB.analytics.data = this.analytics;
    };
  }

  getActiveSlide($slide = false) {
    const $slideActive = $slide || this.$cviceni.querySelector('.active-slide');

    return {
      element: $slideActive,
      id: $slideActive.id,
      index: parseInt($slideActive.getAttribute('data-slide-index'), 10),
    };
  }

  static enableTabControlOnActiveSlide($activeSlide, $oldSlide) {
    const $activeTextareas = $activeSlide.querySelectorAll('textarea');
    const $oldTextareas = $oldSlide.querySelectorAll('textarea');

    $oldTextareas.forEach(($textarea) => {
      $textarea.setAttribute('tabindex', -1);
    });
    $activeTextareas.forEach(($textarea) => {
      $textarea.setAttribute('tabindex', 0);
    });
  }

  loopNavButtons() {
    this.nav.$prev.forEach(($prev) => {
      $prev.addEventListener('click', this._clickPrev, false);
    });
    this.nav.$next.forEach(($next) => {
      $next.addEventListener('click', this._clickNext, false);
    });

    hotkeys('left, shift+space, shift+enter', (event) => {
      this._clickPrev(event);
    });
    hotkeys('right, space, enter', (event) => {
      this._clickNext(event);
    });

    this.buttonFeedbackOnHover();
  }

  _both() {
    this.header();
    Navigation.hideAllOpenComments();
  }

  static hideAllOpenComments() {
    const $comments = document.querySelectorAll('.comment:not(.hidden)');

    $comments.forEach(($comment) => {
      $comment.querySelector('.close').click();
    });
  }

  _clickPrev(event) {
    // get active slide on every click
    const $activeSlide = this.$cviceni.querySelector('.active-slide');
    const $previousSlide = $activeSlide.previousElementSibling;
    const $previousToActiveSlide = this.$cviceni.querySelector('.previous-to-active-slide');

    // zkontroluj zda nějaký další slajd existuje
    if (!__hasClass($previousSlide, 'slide')) {
      // pokud ne
      // __addClass($prev, 'is-hidden');
    } else {
      // pokud ano

      // slides
      __removeClass($activeSlide, 'active-slide');
      __addClass($activeSlide, 'next-slide');

      __removeClass($previousSlide, 'previous-slide');
      __addClass($previousSlide, 'active-slide');

      Navigation.enableTabControlOnActiveSlide($previousSlide, $activeSlide);

      if ($previousToActiveSlide) {
        __removeClass($previousToActiveSlide, 'previous-to-active-slide');
      }
      if ($previousSlide.previousElementSibling) {
        __addClass($previousSlide.previousElementSibling, 'previous-to-active-slide');
      }

      // dispatch event that slide has changed
      __dispatchEvent(this.$cviceni, 'slide.change', {}, { slide: { active: this.getActiveSlide($previousSlide) } });

      // zaznam do analytiky:
      this.analytics = HISTORYLAB.analytics.data;
      this.analytics.activity.length += 1;
      this.analytics.activity.steps[this.analytics.activity.length] = {
        type: 'P',
        slide: parseInt($previousSlide.getAttribute('data-slide-index'), 10),
        time: Math.round(performance.now()) - this.analytics.time.start.time,
        date: Date.now(),
      };
      const interaction = {
        type: event.type,
      };
      switch (event.type) {
        case 'click':
          interaction.name = event.currentTarget.getAttribute('data-feedback-button') ? 'feedback' : 'navigation';
          break;

        case 'keydown':
          interaction.name = event.code;
          break;

        default:
          break;
      }
      this.analytics.activity.steps[this.analytics.activity.length].interaction = interaction;
      HISTORYLAB.analytics.data = this.analytics;

      this._both();
      // save slide to history
      this._saveActiveSlideToHistory();
    }

    event.preventDefault();
  }

  _clickNext(event) {
    // get active slide on every click
    const $activeSlide = this.$cviceni.querySelector('.active-slide');
    // const $previousSlide = $activeSlide.prevElementSibling;
    const $previousToActiveSlide = this.$cviceni.querySelector('.previous-to-active-slide');
    const $nextSlide = $activeSlide.nextElementSibling;
    // const $previousSlide = $activeSlide.previousElementSibling;

    // zkontroluj zda nějaký další slajd existuje
    if (!__hasClass($nextSlide, 'slide')) {
      // pokud ne
      // __addClass($next, 'is-hidden');
    } else {
      // pokud ano

      // slides
      __removeClass($activeSlide, 'active-slide');
      __addClass($activeSlide, 'previous-slide previous-to-active-slide');

      __removeClass($nextSlide, 'next-slide');
      __addClass($nextSlide, 'active-slide visited-slide');

      Navigation.enableTabControlOnActiveSlide($nextSlide, $activeSlide);

      if ($previousToActiveSlide) {
        __removeClass($previousToActiveSlide, 'previous-to-active-slide');
      }

      // dispatch event that slide has changed
      __dispatchEvent(this.$cviceni, 'slide.change', {}, { slide: { active: this.getActiveSlide($nextSlide) } });

      // zaznam do analytiky:
      this.analytics = HISTORYLAB.analytics.data;
      this.analytics.activity.length += 1;
      this.analytics.activity.steps[this.analytics.activity.length] = {
        type: 'D',
        slide: parseInt($nextSlide.getAttribute('data-slide-index'), 10),
        time: Math.round(performance.now()) - this.analytics.time.start.time,
        date: Date.now(),
      };
      const interaction = {
        type: event.type,
      };
      switch (event.type) {
        case 'click':
          interaction.name = event.currentTarget.getAttribute('data-feedback-button') ? 'feedback' : 'navigation';
          break;

        case 'keydown':
          interaction.name = event.code;
          break;

        default:
          break;
      }
      this.analytics.activity.steps[this.analytics.activity.length].interaction = interaction;
      HISTORYLAB.analytics.data = this.analytics;

      this._both();
      // save slide to history
      this._saveActiveSlideToHistory();
    }

    event.preventDefault();
  }

  buttonFeedbackOnHover() {
    this.$buttonFeedbacks.forEach(($buttonFeedback) => {
      const $buttonFeedbackText = $buttonFeedback.querySelector('span');
      const textDefault = JSON.parse($buttonFeedback.getAttribute('data-feedback-button'));
      let textCache = '';

      $buttonFeedback.addEventListener('mouseenter', (event) => {
        textCache = $buttonFeedbackText.innerText;
        const width = $buttonFeedback.offsetWidth;

        if (textCache !== textDefault) {
          $buttonFeedbackText.innerText = textDefault.hover;

          if (width > $buttonFeedback.offsetWidth) {
            $buttonFeedback.style.width = `${width}px`;
          }
        }
      });

      $buttonFeedback.addEventListener('mouseleave', (event) => {
        if (textCache !== textDefault) {
          $buttonFeedback.style.width = '';
          $buttonFeedbackText.innerText = textCache;
        }
      });
    });
  }

  _saveActiveSlideToHistory() {
    const activeSlideHash = `#${this.$cviceni.querySelector('.active-slide').id}`;
    // https://stackoverflow.com/a/14690177/2631749
    if (window.history.pushState) {
      window.history.pushState(null, null, activeSlideHash);
    } else {
      window.location.hash = activeSlideHash;
    }
  }
}
